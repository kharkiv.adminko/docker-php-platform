# Docker platform

## Description

This platform allow you to run php code with mysql database support in docker containers without any installing on your machine

## Requirements

- [Docker](https://docs.docker.com/engine/install/)
- [Docker-compose](https://docs.docker.com/compose/install/)

## Components

- PHP 7.4.29 with xDebug
- MySQL 8
- Adminer can be accessed [here](http://localhost:8080/)
- Nginx can be accessed [here](http://localhost/)

## Installation

1. Clone repository
1. Move your files to `app` folder
1. Change configuration if needed in `.env` file
1. Run application with `docker-compose up -d`
